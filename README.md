Dynamic Battery is an extension for GNOME Shell that makes battery indicator icon show actual charge level,
making it far more accurate than the default 4-state icon.

![Preview](preview.png)

# Themes

Since in almost every icon theme the battery looks differently, the extension supports for several icon themes:
* [Adwaita](https://github.com/GNOME/adwaita-icon-theme)
* [Arc](https://github.com/horst3180/arc-icon-theme)
* [Elementary](https://github.com/elementary/icons)
* [Faba](https://github.com/snwh/faba-icon-theme)
* [Faenza](https://code.google.com/archive/p/faenza-icon-theme)
* [Numix](https://github.com/numixproject/numix-icon-theme)
* [Papirus](https://github.com/PapirusDevelopmentTeam/papirus-icon-theme)
* [Suru](https://github.com/Ubuntu/suru-icon-theme)

All icons are taken from their respective themes and modified, so all credits go to their respective authors.

To request support of any other themes, open an issue here. The only condition is that it should be under
a free license so I can modify and bundle the battery icons with the extension.

# License

The code is licensed under GPLv2+.

Icons in dynamic_battery@exalm/icons are licensed according to their themes' respective licenses.

# Preview mode

There is a preview mode available in the settings. It allows spoofing battery data temporarily to check out
how the indicator looks with given percentage and charging mode.

# Todo

* Autodetect icon theme
* Allow to change "low" and "critical" thresholds, which are currently 30% and 10% respectively
* Fix bugs
