const Clutter = imports.gi.Clutter;
const Cogl = imports.gi.Cogl;
const Lang = imports.lang;
const Signals = imports.signals;
const St = imports.gi.St;
const UPower = imports.gi.UPowerGlib;

const ExtensionUtils = imports.misc.extensionUtils;
const Ext = ExtensionUtils.getCurrentExtension();
const Helpers = Ext.imports.helpers;

const CAUTION_CHARGE_LEVEL = 10;
const LOW_CHARGE_LEVEL = 30;

// Based on screenShield.js#Arrow because it is the
// only example of implementing shadow in the Shell
var AbstractBatteryIcon = new Lang.Class({
    Name: "AbstractBatteryIcon",
    Abstract: true,
    Extends: St.Bin,

    _init: function(params) {
        this.parent(params);
        this.x_fill = this.y_fill = true;
        this.add_style_class_name("dynamic-battery");

        this._drawingArea = new St.DrawingArea();
        this._drawingArea.connect("repaint", Lang.bind(this, this._drawIcon));
        this._drawingArea.connect("style-changed", Lang.bind(this, this._updateSize));
        this.child = this._drawingArea;

        this._shadowHelper = null;
    },

    _updateSize: function() {
        let node = this.get_theme_node();
        let iconSize = Math.round(node.get_length("icon-size"));
        this._drawingArea.width = iconSize;
    },

    _drawIcon: function(icon) {
        if (!this._proxy)
            return

        let cr = icon.get_context();
        let [w, h] = icon.get_surface_size();
        let node = this.get_theme_node();

        let iconSize = Math.round(node.get_length("icon-size"));
        cr.translate(0, Math.floor((h - iconSize) / 2));

        this.drawBattery(cr, iconSize, node, this._proxy);

        cr.$dispose();
    },

    drawBattery: function(cr, size, themeNode, proxy) {},

    vfunc_get_paint_volume: function(volume) {
        if (!this.parent(volume))
            return false;

        if (!this._shadow)
            return true;

        let shadow_box = new Clutter.ActorBox();
        this._shadow.get_box(this._drawingArea.get_allocation_box(), shadow_box);

        volume.set_width(Math.max(shadow_box.x2 - shadow_box.x1, volume.get_width()));
        volume.set_height(Math.max(shadow_box.y2 - shadow_box.y1, volume.get_height()));

        return true;
    },

    vfunc_style_changed: function() {
        let node = this.get_theme_node();
        this._shadow = node.get_shadow("icon-shadow");
        if (this._shadow)
            this._shadowHelper = St.ShadowHelper.new(this._shadow);
        else
            this._shadowHelper = null;

        this.parent();
    },

    vfunc_paint: function() {
        if (this._shadowHelper) {
            this._shadowHelper.update(this._drawingArea);

            let allocation = this._drawingArea.get_allocation_box();
            let paintOpacity = this._drawingArea.get_paint_opacity();
            let framebuffer = Cogl.get_draw_framebuffer();

            this._shadowHelper.paint(framebuffer, allocation, paintOpacity);
        }

        this._drawingArea.paint();
    },

    updateBattery: function(proxy) {
        this._proxy = proxy;
        // Explicitly update shadow
        this.vfunc_style_changed();
        this.child.queue_repaint();
    }
});

var DynamicBatteryIcon = new Lang.Class({
    Name: "DynamicBatteryIcon",
    Abstract: true,
    Extends: AbstractBatteryIcon,

    _init: function(params) {
        this.parent(params);
        this.setTheme("adwaita");
    },

    setTheme: function(newTheme) {
        if (newTheme == this._currentTheme)
            return;

        this.add_style_class_name(newTheme);
        if (this._currentTheme)
            this.remove_style_class_name(this._currentTheme);
        this._currentTheme = newTheme;
    },

    drawBattery: function(cr, size, themeNode, proxy) {
        if (!proxy || !proxy.IsPresent) {
            // Not configurable via CSS
            Helpers.drawIcon(cr, "system-shutdown-symbolic", size, themeNode);
        } else {
            switch(proxy.State) {
                case UPower.DeviceState.EMPTY:
                    Helpers.drawIconFromCSS(cr, "-dynamic-battery-icon-empty", size, themeNode, this._currentTheme);
                    return;
                case UPower.DeviceState.FULLY_CHARGED:
                    Helpers.drawIconFromCSS(cr, "-dynamic-battery-icon-full-charged", size, themeNode, this._currentTheme);
                    return;
                case UPower.DeviceState.CHARGING:
                case UPower.DeviceState.PENDING_CHARGE:
                    if (proxy.Percentage <= 0)
                        // This isn't actually used by default, but some themes have unique icons for this nevertheless
                        Helpers.drawIconFromCSS(cr, "-dynamic-battery-icon-empty-charging", size, themeNode, this._currentTheme);
                    else
                        this._drawDynamicBattery(cr, size, themeNode, proxy.Percentage, true);
                    return;
                case UPower.DeviceState.DISCHARGING:
                case UPower.DeviceState.PENDING_DISCHARGE:
                    this._drawDynamicBattery(cr, size, themeNode, proxy.Percentage, false);
                    return;
                default:
                    Helpers.drawIconFromCSS(cr, "-dynamic-battery-icon-missing", size, themeNode, this._currentTheme);
                    return;
            }
        }
    },

    _drawDynamicBattery: function(cr, size, themeNode, percentage, charging) {
        let caution = percentage <= CAUTION_CHARGE_LEVEL;
        let low = percentage <= LOW_CHARGE_LEVEL;
        let frac = 1 - percentage / 100;

        let bgProp = "-dynamic-battery-bg";
        let fgProp = "-dynamic-battery-fg";
        if (caution) {
            bgProp += "-caution";
            fgProp += "-caution";
        } else if (low) {
            bgProp += "-low";
            fgProp += "-low";
        }
        if (charging) {
            bgProp += "-charging";
            fgProp += "-charging";
        } else if (!caution && !low) {
            bgProp += "-normal";
            fgProp += "-normal";
        }

        let baseSize = 16 * St.ThemeContext.get_for_stage(global.stage).scale_factor;
        // Cairo directs Y axis as top to bottom, while CSS directs it bottom to top, as in Inkscape
        let rectT = (1 - themeNode.get_length("-dynamic-battery-bottom") / baseSize) * size;
        let rectB = (1 - themeNode.get_length("-dynamic-battery-top") / baseSize) * size;
        let rectL = themeNode.get_length("-dynamic-battery-left") / baseSize * size;
        let rectR = themeNode.get_length("-dynamic-battery-right") / baseSize * size;
        let dir = Helpers.getString(themeNode, "-dynamic-battery-direction");

        if (dir == "top-to-bottom")
            rectB += (rectT - rectB) * frac;
        if (dir == "bottom-to-top")
            rectT -= (rectT - rectB) * frac;
        if (dir == "left-to-right")
            rectL += (rectR - rectL) * frac;
        if (dir == "right-to-left")
            rectR -= (rectR - rectL) * frac;

        Helpers.setSourceIconFromCSS(cr, bgProp, size, themeNode, this._currentTheme);
        cr.rectangle(0, 0, size, size);
        if (!Helpers.getBoolean(themeNode, "-dynamic-battery-draw-bg-behind-fg")) {
            cr.moveTo(rectL, rectB);
            cr.lineTo(rectL, rectT);
            cr.lineTo(rectR, rectT);
            cr.lineTo(rectR, rectB);
        }
        cr.closePath();
        cr.fill();

        Helpers.setSourceIconFromCSS(cr, fgProp, size, themeNode, this._currentTheme);

        cr.moveTo(rectL, rectB);
        cr.lineTo(rectL, rectT);
        cr.lineTo(rectR, rectT);
        cr.lineTo(rectR, rectB);
        cr.closePath();
        cr.fill();
    }
});
