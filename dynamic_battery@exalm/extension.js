const Lang = imports.lang;
const Gtk = imports.gi.Gtk;
const Meta = imports.gi.Meta;
const Shell = imports.gi.Shell;
const Signals = imports.signals;
const St = imports.gi.St;

const Main = imports.ui.main;
const PopupMenu = imports.ui.popupMenu;
const Power = imports.ui.status.power;

const ExtensionUtils = imports.misc.extensionUtils;
const Ext = ExtensionUtils.getCurrentExtension();
const Convenience = Ext.imports.convenience;
const BatteryIcon = Ext.imports.batteryIcon;
const Themes = Ext.imports.themes;

const UPower = imports.gi.UPowerGlib;

let _settings;
let _settingsChangedId;
let _theme;

let _origSync;
let _indicator;
let _syncId1, _syncId2;

let _previewMode, _previewPercentage, _previewCharging;

const SHOW_BATTERY_PERCENTAGE = 'show-battery-percentage';

function _sync() {
    if (!this._proxy.IsPresent)
        return;

    let proxy = this._proxy;
    if (_previewMode) {
        proxy = {
            IsPresent: true,
            Percentage: _previewPercentage,
            State: _previewCharging ? UPower.DeviceState.CHARGING : UPower.DeviceState.DISCHARGING
        };
        if (_previewPercentage == 0 && !_previewCharging)
            proxy.State = UPower.DeviceState.EMPTY
        if (_previewPercentage == 100 && _previewCharging)
            proxy.State = UPower.DeviceState.FULLY_CHARGED
    }

    this._indicator.updateBattery(proxy);
    this.menu.firstMenuItem.icon.updateBattery(proxy);
}

function init(meta) {
    _indicator = Main.panel.statusArea.aggregateMenu._power;
    _origSync = Lang.bind(_indicator, _indicator._sync);

    _settings = Convenience.getSettings();
    _previewMode = _settings.get_boolean("test-mode");
    _previewCharging = _settings.get_boolean("test-charging");
    _previewPercentage = _settings.get_int("test-percentage");
    _theme = Themes.getThemeFromSettings(_settings);

    Gtk.IconTheme.get_default().append_search_path(meta.path + "/icons");
}

function _settingsChanged() {
    _previewMode = _settings.get_boolean("test-mode");
    _previewCharging = _settings.get_boolean("test-charging");
    _previewPercentage = _settings.get_int("test-percentage");
    _theme = Themes.getThemeFromSettings(_settings);

    _indicator._indicator.setTheme(_theme);
    _indicator.menu.firstMenuItem.icon.setTheme(_theme);
    Lang.bind(_indicator, _sync)(); 
}

function enable() {

    let icon = new BatteryIcon.DynamicBatteryIcon({
        style_class: "system-status-icon"
    });
    icon.setTheme(_theme);
    icon.connect("notify::visible", Lang.bind(_indicator, _indicator._syncIndicatorsVisible));
    _indicator.indicators.insert_child_at_index(icon, 0);
    _indicator._indicator.destroy();
    _indicator._indicator = icon;

    let icon2 = new BatteryIcon.DynamicBatteryIcon({
        style_class: "popup-menu-icon"
    });
    icon2.setTheme(_theme);
    _indicator.menu.firstMenuItem.actor.insert_child_at_index(icon2, 1);
    _indicator.menu.firstMenuItem.icon.destroy();
    _indicator.menu.firstMenuItem.icon = icon2;

    if (_indicator._desktopSettings)
        _syncId1 = _indicator._desktopSettings.connect("changed::" +
                SHOW_BATTERY_PERCENTAGE, Lang.bind(_indicator, _sync));
    _syncId2 = _indicator._proxy.connect("g-properties-changed",
            Lang.bind(_indicator, _sync));

    _indicator._sync();
    Lang.bind(_indicator, _sync)();

    _settingsChangedId = _settings.connect("changed", _settingsChanged);
}

function disable() {
    _indicator._indicator.destroy();

    _indicator._indicator = _indicator._addIndicator();

    let icon = new St.Icon({
        style_class: "popup-menu-icon"
    });
    _indicator.menu.firstMenuItem.actor.insert_child_at_index(icon, 1);
    _indicator.menu.firstMenuItem.actor.remove_actor(_indicator.menu.firstMenuItem.icon);
    _indicator.menu.firstMenuItem.icon = icon;

    if (_indicator._desktopSettings)
        _indicator._desktopSettings.disconnect(_syncId1);
    _indicator._proxy.disconnect(_syncId2);

    _indicator._sync();

    _settings.disconnect(_settingsChangedId);
}

