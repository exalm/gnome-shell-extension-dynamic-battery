const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

const ExtensionUtils = imports.misc.extensionUtils;
const Ext = ExtensionUtils.getCurrentExtension();
const Convenience = Ext.imports.convenience;
const Themes = Ext.imports.themes;

const Gettext = imports.gettext.domain("dynamic-battery");
const _ = Gettext.gettext;

let _settings;
let _batteryLevelRow, _chargingRow;
let _themeListbox;

function init() {
    _settings = Convenience.getSettings();
}

function _createThemeRow(list, theme) {
    let row = new Gtk.ListBoxRow();
    let label = new Gtk.Label({
        label: theme.name,
        halign: Gtk.Align.START,
        margin_top: 2,
        margin_bottom: 2,
        margin_start: 6
    });
    row.add(label);
    list.add(row);

    return row;
}

function _headerFunc(row, before) {
    if (!before || row.get_header())
        return;

    row.set_header(new Gtk.Separator({
        orientation: Gtk.Orientation.HORIZONTAL
    }));
}

function _settingsChanged(settings, key) {
    if (key == "theme") {
        let theme = Themes.getThemeIndex(settings);
        let row = _themeListbox.get_row_at_index(theme);
        _themeListbox.select_row(row);
        return;
    }
    if (key == "test-mode") {
        let previewMode = settings.get_boolean("test-mode");
        _batteryLevelRow.sensitive = previewMode;
        _chargingRow.sensitive = previewMode;
        return;
    }
}

function buildPrefsWidget() {
    let builder = Gtk.Builder.new_from_file(Ext.path + "/prefs.ui");
    let box = builder.get_object("main_box");

    _themeListbox = builder.get_object("theme_listbox");

    for (let theme of Themes.THEMES)
        _createThemeRow(_themeListbox, theme);

    _settingsChanged(_settings, "theme");
    _themeListbox.connect("row-selected", (box, row) => {
        _settings.set_string("theme", Themes.THEMES[row.get_index()].val);
    });

    builder.get_object("preview_listbox").set_header_func(_headerFunc);

    let enablePreviewModeSwitch = builder.get_object("enable_preview_mode_switch");
    _settings.bind("test-mode", enablePreviewModeSwitch, "active", Gio.SettingsBindFlags.DEFAULT);

    let batteryLevelAdj = builder.get_object("preview_battery_level_adjustment");
    _settings.bind("test-percentage", batteryLevelAdj, "value", Gio.SettingsBindFlags.DEFAULT);

    let chargingSwitch = builder.get_object("preview_charging_switch");
    _settings.bind("test-charging", chargingSwitch, "active", Gio.SettingsBindFlags.DEFAULT);

    _batteryLevelRow = builder.get_object("preview_battery_level_row");
    _chargingRow = builder.get_object("preview_charging_row");

    _settingsChanged(_settings, "test-mode");
    _settings.connect("changed", _settingsChanged);

    box.show_all();
    return box;
}
