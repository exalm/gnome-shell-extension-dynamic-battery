// A list of themes

const THEMES = [
    { val: "adwaita",    name: "Adwaita" },     // LGPLv3 https://github.com/GNOME/adwaita-icon-theme
    { val: "adwaita2",   name: "Adwaita (alternative)" }, // Based on Adwaita
    { val: "arc",        name: "Arc" },         // GPLv3  https://github.com/horst3180/arc-icon-theme
    { val: "elementary", name: "Elementary" },  // GPLv3  https://github.com/elementary/icons
    { val: "faba",       name: "Faba" },        // GPLv3  https://github.com/snwh/faba-icon-theme
    { val: "faba-mono",  name: "Faba (alternative)" }, // Alternative battery icons from Faba, used in Faba-Mono
    { val: "faenza",     name: "Faenza" },      // GPLv3  https://code.google.com/archive/p/faenza-icon-theme
    // aliases: Mint-X
    { val: "numix",      name: "Numix" },       // GPLv3  https://github.com/numixproject/numix-icon-theme
    { val: "papirus",    name: "Papirus" },     // LGPLv3 https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
    // aliases: Paper; Flat Remix
    { val: "suru",       name: "Suru" }         // GPLv3  https://github.com/Ubuntu/suru-icon-theme
];

function getThemeFromSettings(settings) {
    let theme = settings.get_string("theme");

    if (theme == "mintx") {
        settings.set_string("theme", "faenza");
        return "faenza";
    }

    if (theme == "paper") {
        settings.set_string("theme", "papirus");
        return "papirus";
    }

    return theme;
}

function getThemeIndex(settings) {
    let val = getThemeFromSettings(settings);
    for (let i = 0; i < THEMES.length; i++)
        if (THEMES[i].val == val)
            return i;

    return -1;
}
